export const GET_DATA = 'GET_DATA';
export const HANDLE_WISHLIST = 'HANDLE_WISHLIST';
export const HANDLE_CART = 'HANDLE_CART';
export const HANDLE_MODAL = 'HANDLE_MODAL';
export const TOGGLE_MODAL = 'TOGGLE_MODAL';
export const ADD_CONTACTS = 'ADD_CONTACTS';
export const HANDLE_ORDER = 'HANDLE_ORDER';
