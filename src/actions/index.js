import { GET_DATA, HANDLE_WISHLIST, HANDLE_CART, TOGGLE_MODAL, HANDLE_MODAL, ADD_CONTACTS, HANDLE_ORDER } from "./types";
import store from "../store";

const isLocalStorageEnabled = () => {
  try {
    const key = `__storage__test`;
    window.localStorage.setItem(key, null);
    window.localStorage.removeItem(key);
    return true;
  } catch (e) {
    return false;
  }
};

export const getData = () => async (dispatch) => {
  console.log("action getData");
  let products = [],
    cart = [],
    wishlist = [],
    modals = []
  if (isLocalStorageEnabled()) {
  if (localStorage.getItem("wishlist")) {
    wishlist = await JSON.parse(localStorage.getItem("wishlist"));
  }

  if (localStorage.getItem("cart")) {
    cart = await JSON.parse(localStorage.getItem("cart"));
  }
  }
  products = await fetch(
    "data.json", // from /public folder
    {
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    }
  )
    .then((response) => response.json())
        
    modals = await fetch(
      "modals.json", // from /public folder
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      }
    ).then(response => response.json())

  dispatch({
    type: GET_DATA,
    payload: {
      products: products,
      cart: cart,
      wishlist: wishlist,
      modals: modals,
    }
  }) 

};

export const handleWishlist = (wishlist) => {
  if (isLocalStorageEnabled()) {
    localStorage.setItem("wishlist", JSON.stringify(wishlist));
  }
  return {
    type: HANDLE_WISHLIST,
    payload: wishlist
  }
}

export const handleCart = (cart) => {
  if (isLocalStorageEnabled()) {
    localStorage.setItem("cart", JSON.stringify(cart));
  }
  return {
    type: HANDLE_CART,
    payload: cart
  }
}

export const toggleModal = () => {
  return {
    type: TOGGLE_MODAL,
  }
}

export const handleModal = (modal, action) => {
  const modals = store.getState().product.modals
  console.log("modal", modals)
  const thisModal = modals.filter(item => item.id === modal)
  return {
    type: HANDLE_MODAL,
    payload: {
      ...thisModal[0], action: action
    }
  }
}

export const addContacts = (contacts) => {
  return {
      type: ADD_CONTACTS,
      payload: contacts
  }
}

export const handleOrder = (cart, contacts) => {
  const products = store.getState().product.products;
  const cartItems = cart.map(item => {
    const itemNew = products.filter(product => product.id === item)
    return itemNew[0]
  })
  console.log("your products: \n", cartItems, " \n your contacts: \n", contacts)
  if (isLocalStorageEnabled()) {
    localStorage.removeItem("cart")
  }
  return {
    type: HANDLE_ORDER,
    payload: {
      cart: [],
      contacts: {},
    }
  }
}