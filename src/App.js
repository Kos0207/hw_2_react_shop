import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";
import { getData } from "./actions";
import "./App.css";
import Header from "./components/Header";
import Products from "./components/Products";
import Wishlist from "./pages/Wishlist";
import Cart from "./pages/Cart";
import NotFound from "./pages/NotFound";
import Modal from "./components/Modal";


function App() {
  store.dispatch(getData());

  return (
    <Provider store={store}>
      <BrowserRouter>
        <div className="App">
          <Header />
          <Routes>
            <Route path="/" element={<Products />} />

            <Route path="/wishlist" element={<Wishlist />} />

            <Route path="/cart" element={<Cart />} />

            <Route path="*" element={<NotFound />} />
          </Routes>
          <Modal />
        </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
