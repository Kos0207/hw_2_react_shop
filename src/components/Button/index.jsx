import React from 'react'
import {connect} from "react-redux"
import {toggleModal, handleModal} from '../../actions'
import PropTypes from 'prop-types'
import './index.scss'

const Button = ({buttonText, backgroundColor, onClick, modal, toggleModal, handleModal}) => {
  return (
    <button data-testid="button" type='button' className={`btn ${backgroundColor}`} onClick={ modal ? (() => {
        handleModal(modal, onClick)
        toggleModal()
      }) : onClick} 
      modal={modal}>{buttonText}</button>
  )
}

Button.propTypes = {
  buttonText: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  modal: PropTypes.string,
  toggleModal: PropTypes.func.isRequired,
  handleModal: PropTypes.func.isRequired
  }

  Button.defaultProps = {
    modal: null
   
  }



export default connect(null, {toggleModal, handleModal})(Button);


