import React from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import "./index.scss";
import { addContacts } from "../../actions";
import { Formik, Form, Field, ErrorMessage } from "formik";
// import { PatternFormat } from "react-number-format";
import * as Yup from "yup";

const signInSchema = Yup.object().shape({
  name: Yup.string()
    .required("Name is required")
    .min(2, "The name must consist of at least 2 letters")
    .max(25, "Maximum number of letters"),
  lastName: Yup.string()
    .required("Lastname is required")
    .min(1, "The lastname must consist of at least 1 letters")
    .max(45, "Maximum number of letters"),
  age: Yup.number("Agу must be a number")
    .required("Age is required")
    .positive()
    .min(18, "your age is minimum 18 years"),
  deliveryAddress: Yup.string()
    .required("Address is required")
    .min(2, "minimum number of symbols"),
  phone: Yup.string("Phone must be a number")
    .required("Phone is required")
    .length(9, "Enter the correct phone number (9 numbers)"),

    
});

const ContactForm = ({ addContacts, contacts }) => (
  <div className="contactform">
    <h2 className="contactform__title">Add Contacts</h2>
    <Formik
      initialValues={{
        name: "",
        lastName: "",
        age: null,
        deliveryAddress: "",
        phone: null,
      }}
      validationSchema={signInSchema}
      onSubmit={(values) => {
        addContacts(values);
      }}
    >
      {({ errors, touched }) => (
        <Form className="contactform__form">
          <div className="contactform__form-datas">
          <label className="contactform__form-datas-label">Name</label>
          <Field
            type="text"
            name="name"
            className={
              errors.name && touched.name
                ? "form-control is-invalid"
                : "form-control"
            }
          />
          <ErrorMessage
            name="name"
            component="div"
            className="invalid-feedback"
          />
          </div>

          <div className="contactform__form-datas">
          <label className="contactform__form-datas-label">Lastname</label>
          <Field
            type="text"
            name="lastName"
            className={
              errors.lastName && touched.lastName
                ? "form-control is-invalid"
                : "form-control"
            }
          />
          <ErrorMessage
            name="lastName"
            component="div"
            className="invalid-feedback"
          />
          </div>

          <div className="contactform__form-datas">
          <label className="contactform__form-datas-label">Age</label>
          <Field
            type="text"
            name="age"
            className={
              errors.age && touched.age
                ? "form-control is-invalid"
                : "form-control"
            }
          />
          <ErrorMessage
            name="age"
            component="div"
            className="invalid-feedback"
          />
          </div>

          <div className="contactform__form-datas">
          <label className="contactform__form-datas-label">Address of Delivery</label>
          <Field
            type="text"
            name="deliveryAddress"
            className={
              errors.deliveryAddress && touched.deliveryAddress
                ? "form-control is-invalid"
                : "form-control"
            }
          />
          <ErrorMessage
            name="deliveryAddress"
            component="div"
            className="invalid-feedback"
          />
          </div>

          <div className="contactform__form-datas">
          <label className="contactform__form-datas-label">Phone</label>
          <Field
            type="text"
            name="phone"
            className={
              errors.phone && touched.phone
                ? "form-control is-invalid"
                : "form-control"
              }

          />
            {/* <PatternFormat 
             name="phone"
            className={ errors.phone && touched.phone
              ? "form-control is-invalid"
              : "form-control"}
            format="(###) ### ## ##" allowEmptyFormatting={true} mask={"#"}/> */}
          <ErrorMessage
            name="phone"
            component="div"
            className="invalid-feedback"
          />
          </div>

          <button className="btn primary" type="submit">
            Save Contacts
          </button>
          {contacts.hasOwnProperty('name') ? <p>Saved Contacts</p> : ""} 

        </Form>
      )}
    </Formik>
  </div>
);

const mapStateToProps = (state) => ({
  contacts: state.product.contacts,
});

ContactForm.propTypes = {
  contacts: PropTypes.array.isRequired,
};

ContactForm.defaultProps = {
  contacts: [],
};

export default connect(mapStateToProps, { addContacts })(ContactForm);
