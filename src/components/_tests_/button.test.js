import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import Button from "../Button";

afterEach(cleanup);

describe("Button", () => {
    
  test("renders button", () => {
    render(<Button />);
    const button = screen.getByText("ButtonText");
    expect(button).toBeInTheDocument();
    fireEvent.click()
  });

  test("renders button with props", () => {
    render(<Button className="btn" /> );
    const button = screen.getByText("ButtonText");
    expect(button).toBeInTheDocument();
    expect(button).toHaveClass("btn");
  });
});
