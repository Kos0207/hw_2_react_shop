import { cleanup } from "@testing-library/react";
import renderer from "react-test-renderer";
import Header from "../Header";

afterEach(cleanup);

test ("snapshot header", ()=>{
    const tree = renderer.create(<Header />).toJSON();
    expect(tree).toMatchSnapshot();


})