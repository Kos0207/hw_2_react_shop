import { render, screen, cleanup } from "@testing-library/react";
import Modal from "../Modal";

afterEach(cleanup);

describe("Modal", () => {
  test("Modal - Renders Modal Component", () => {
    render(<Modal />);
    const modal = screen.getByTestId("modal");
    expect(modal).toBeInTheDocument();
  });

    test("Modal - Renders Modal Component with title, button", () => {
    render(<Modal title="Modal Title" button="Modal Button" />);
    const modal = screen.getByTestId("modal");
    expect(modal).toBeInTheDocument();
  });
  // test("Modal - simple props", () => {
  //     const modalProps = { activeModal, isModalOpen, toggleModal }
  //     render(
  //       <Modal {...modalProps}
  //       />
  //     );
  //     const modal = screen.getByTestId("modal");
  //     expect(modal).toBeInTheDocument();

  //   });
});
