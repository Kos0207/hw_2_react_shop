import React from "react";
import Button from "../Button";
import { connect } from "react-redux";
import { toggleModal } from "../../actions";
import PropTypes from 'prop-types';
import "./index.scss";

const Modal = ({ activeModal, isModalOpen, toggleModal }) => {
  const { header, text, closeButton, actionBtn, action } = activeModal;
  const openModal = () => toggleModal();
  const clickFunction = action ? action : openModal
  const combiningFunctions = (e) => {openModal(e); clickFunction(e)}

  return (
    <>
      {isModalOpen && (
        <div className="modal show" data-testid="modal">
          <div className="modal__overlay" onClick={openModal}></div>
          <div className={"modal__content"}>
            <div className={"modal__content-top"}>
              <h3 className={"modal__content-header"}>{header}</h3>
              <div className={"modal__close-btn close-btn"} onClick={openModal}>
                &times;
              </div>
            </div>
            <p className={"modal__content-text"}>{text}</p>
            <div className={"modal__content-bottom"}>
              <button className={actionBtn.className} onClick={combiningFunctions}>
                {actionBtn.text}
              </button>
              {closeButton ? (
                <Button
                  buttonText="Cancel"
                  backgroundColor="secondary"
                  onClick={openModal}
                />
              ) : null}
            </div>
          </div>
        </div>
      )}
    </>
  );
};
const mapStateToProps = (state) => ({
  activeModal: state.modal.activeModal,
  isModalOpen: state.modal.isModalOpen,
});

Modal.propTypes = {
    header: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  closeButton: PropTypes.bool,
  actionBtn: PropTypes.object,
  action: PropTypes.func
}

Modal.defaultProps = {
   title: 'Modal',
  text: '...',
  closeButton: true,
  actionBtn: {}
}


export default connect(mapStateToProps, { toggleModal })(Modal);
