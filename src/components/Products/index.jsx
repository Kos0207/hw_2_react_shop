import React from 'react'
import Product from '../Product'
import { connect } from 'react-redux'
import './index.scss'

const Products = ({products})=> {
   
  return (
    <>
    <h2 className="title-products">Our Products</h2>
    <div className='products'> 
      {products.map((product)=> (
          <Product
          key={product.id}
          product={product}
          />
      ))}
       
    </div>
    </>
  )
}
const mapStateToProps = (state) => ({
  products: state.product.products
});

export default connect(mapStateToProps, null)(Products);