import React from "react";
import PropTypes from 'prop-types';
import { NavLink } from "react-router-dom";
import "./index.scss";
import {connect} from "react-redux";

const Header = ({ wishlist, cart }) => {
  return (
    <header className="header">
      <div className="header__logo-name">Market-Stock </div>
      <ul className="header__nav">
        <li>
          <NavLink to="/">Home</NavLink>
        </li>
        <li>
          <NavLink to="/wishlist">wishlist</NavLink>
        </li>
        <li>
          <NavLink to="/cart">cart</NavLink>
        </li>
      </ul>
      <div className="header__bascket">
        <div className="symbol-star">
          {wishlist.length > 0 ? (
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
              <path d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" />
            </svg>
          ) : (
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
              <path d="M528.1 171.5L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6zM388.6 312.3l23.7 138.4L288 385.4l-124.3 65.3 23.7-138.4-100.6-98 139-20.2 62.2-126 62.2 126 139 20.2-100.6 98z" />
            </svg>
          )}
          <span className="symbol-star-amount">
            {wishlist.length > 0 ? wishlist.length : ""}
          </span>
        </div>

        <NavLink to="/cart"><div className="header__bascket-img">
          <svg
            width="16"
            height="20"
            viewBox="0 0 16 20"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M14 4H12C12 1.79 10.21 0 8 0C5.79 0 4 1.79 4 4H2C0.9 4 0 4.9 0 6V18C0 19.1 0.9 20 2 20H14C15.1 20 16 19.1 16 18V6C16 4.9 15.1 4 14 4ZM8 2C9.1 2 10 2.9 10 4H6C6 2.9 6.9 2 8 2ZM14 18H2V6H4V8C4 8.55 4.45 9 5 9C5.55 9 6 8.55 6 8V6H10V8C10 8.55 10.45 9 11 9C11.55 9 12 8.55 12 8V6H14V18Z"
              fill="#EEEEEE"
            ></path>
          </svg>
        </div>

        <span className="header__bascket-text">
          Кошик {cart.length > 0 ? cart.length : ""}
        </span></NavLink>
        
      </div>
    </header>
  );
};

const mapStateToProps = (state) => ({
  wishlist: state.product.wishlist,
  cart: state.product.cart

})

Header.propTypes = {
  wishlist: PropTypes.array.isRequired,
  cart: PropTypes.array.isRequired
  }

  Header.defaultProps = {
    wishlist: [],
    cart: []
  }

export default connect(mapStateToProps, null)(Header);
