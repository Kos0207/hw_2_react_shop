import React, { useEffect, useState } from "react";
import "./index.scss";
import PropTypes from 'prop-types';
import Button from "../Button";
// import Modal from "../../components/Modal";
import { useLocation } from "react-router-dom";
import {connect} from "react-redux";
import {handleWishlist} from "../../actions"
import {handleCart} from "../../actions"


const Product = ({ product, wishlist, handleWishlist, cart, handleCart }) => {
  const { id, title, price, image, code, color} = product;
  const [addedWishlist, setAddedWishlist] = useState(false);
  const [addedCart, setAddedCart] = useState(false);
  const page = useLocation().pathname;

  useEffect(() => {
    if (wishlist.includes(id)) {
      setAddedWishlist(true);
    }

    if (cart.includes(id)) {
      setAddedCart(true);
    }
  }, [wishlist, cart, id]);

  const updateWishList = (e) => {
    e.preventDefault();
    setAddedWishlist((prev) => !prev);
    if (!addedWishlist) {
      if (wishlist.length) {
        if (!wishlist.includes(id)) {
          wishlist = [...wishlist, id];
        }
      } else {
        wishlist = [...wishlist, id];
      }
    } else {
      if (wishlist.length) {
        wishlist = wishlist.filter((item) => item !== id);
      }
    }
    handleWishlist(wishlist);
  };

  const updateCart = (e) => {
    e.preventDefault();
    setAddedCart((prev) => !prev);

    if (!addedCart) {
      if (cart.length) {
        if (!cart.includes(id)) {
          cart = [...cart, id];
        }
      } else {
        cart = [...cart, id];
      }
    } else {
      if (cart.length) {
        cart = cart.filter((item) => item !== id);
      }
    }
    handleCart(cart);
  };

  return (
    <div className="product">
      <div className="product__top">
        <div className="product__top-title">{title}</div>
        <div className="symbol-star" onClick={updateWishList}>
          {addedWishlist ? (
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
              <path d="M259.3 17.8L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0z" />
            </svg>
          ) : (
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512">
              <path d="M528.1 171.5L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6zM388.6 312.3l23.7 138.4L288 385.4l-124.3 65.3 23.7-138.4-100.6-98 139-20.2 62.2-126 62.2 126 139 20.2-100.6 98z" />
            </svg>
          )}
        </div>
      </div>
      <div className="product__price">
        <strong>$:</strong> {price}
      </div>
      <div className="product__image">
        <img className="product__image-goods" src={image} alt={title} />
      </div>
      <div className="product__code">
        <strong>Code:</strong> {code}
      </div>
      <div className="product__color">
        <strong>Color:</strong> {color}
      </div>
      {page.includes("cart") ? (
        <Button
        buttonText="Remove"
        onClick={(e)=> updateCart(e)}
        modal="removeProduct"
        backgroundColor="secondary"
        />
      ) : (
       addedCart ? <button type="button" className="btn primary" disabled>in cart</button> : (<Button
        buttonText="Add to cart"
        onClick={(e)=> updateCart(e) }
        modal="addProduct"
        backgroundColor="primary"
        />)
      )}
    </div>
  );
};

const mapStateToProps = (state) => ({
  wishlist: state.product.wishlist,
  cart: state.product.cart
})

Product.propTypes = {
  product: PropTypes.object.isRequired,
  updateWishlist: PropTypes.func.isRequired,
  updateCart: PropTypes.func.isRequired,
  wishlist: PropTypes.array.isRequired,
  cart: PropTypes.array.isRequired
}

Product.defaultProps = {
 product: {}
}
 

export default connect(mapStateToProps, {handleWishlist, handleCart})(Product);
