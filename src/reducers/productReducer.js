import { GET_DATA, HANDLE_WISHLIST, HANDLE_CART, ADD_CONTACTS, HANDLE_ORDER} from "../actions/types";


const initialState = {
  products: [],
  wishlist: [],
  cart: [],
  modals: [],
  contacts: {},
};

export default function productReducer(state = initialState, action) {
  switch (action.type) {
    case GET_DATA:
      console.log("reducer_GET_DATA");
      return {
        ...state,
        products: action.payload.products,
        wishlist: action.payload.wishlist,
        cart: action.payload.cart,
        modals: action.payload.modals,
      };

      case HANDLE_WISHLIST:
      console.log("reducer_HANDLE_WISHLIST");
      return {
        ...state,
        wishlist: action.payload
      };

      case HANDLE_CART:
        console.log("reducer_HANDLE_CART");
        return {
          ...state,
          cart: action.payload
        };

     case ADD_CONTACTS:
      console.log("reducer_ADD_CONTACTS");
      return {
        ...state,
        contacts: action.payload
      };

      case HANDLE_ORDER:
        console.log("reducer_HANDLE_ORDER");
        return {
          ...state,
          cart: action.payload.cart,
          contacts: action.payload.contacts
        };



    default:
      return state;
  }
}
