import { HANDLE_MODAL, TOGGLE_MODAL } from "../actions/types";

const initialState = {
  activeModal: {
    id: "none",
    header: "modal",
    text: "...",
    closeButton: true,
    backgroundColor: "secondary",
    actionBtn: {
      className: "modal__btn",
      text: "yes",
    },
  },
  isModalOpen: false,
};

export default function modalReducer(state = initialState, action) {
  switch (action.type) {
    case HANDLE_MODAL:
      return {
        ...state,
        activeModal: action.payload,
      };

    case TOGGLE_MODAL: {
      return {
        ...state,
        isModalOpen: !state.isModalOpen,
      };
    }
    default:
      return state;
  }
}
