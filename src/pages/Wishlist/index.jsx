import React from "react";
import Product from "../../components/Product";
import { connect } from "react-redux";

const Wishlist = ({ products, wishlist }) => {
  const productsWishlist = products.filter((product) =>
    wishlist.includes(product.id)
  );
  const hasProducts = productsWishlist.length > 0;
  return (
    <>
      <h2 className="title-products">Our Wishlist</h2>
      {hasProducts ? (
        <div className="products">
          {productsWishlist.map((product) => {
            return <Product 
            key={product.id}
            product={product} />;
          })}
        </div>
      ) : (
        <p className="no-items">No products in wishlist</p>
      )}
    </>
  );
};
const mapStateToProps = (state) => ({
  wishlist: state.product.wishlist,
  products: state.product.products,
});
export default connect(mapStateToProps, null)(Wishlist);
