import React from "react";
import Product from "../../components/Product";
import { connect } from "react-redux";
import ContactForm from "../../components/ContactForm";
import {handleOrder} from "../../actions";

const Cart = ({ products, cart, contacts, handleOrder }) => {
  const productsCart = products.filter((product) => cart.includes(product.id));
  const hasProducts = productsCart.length > 0;
  const hasContacts = contacts.hasOwnProperty('name');

  return (
    <>
      <h2 className="title-products">Our Cart</h2>
      {hasProducts ? (
        <>
          <ContactForm />
          <div className="products">
            {productsCart.map((product) => {
              return <Product key={product.id} product={product} />;
            })}
          </div>
          <button type="button" className={!hasContacts ? "btn primary disabled" : "btn primary" } onClick={() => {handleOrder(cart, contacts)}} >Checkout</button>
        </>
      ) : (
        <p className="no-items">No products in cart</p>
      )}
    </>
  );
};
const mapStateToProps = (state) => ({
  cart: state.product.cart,
  products: state.product.products,
  contacts: state.product.contacts,
});
export default connect(mapStateToProps, {handleOrder})(Cart);
